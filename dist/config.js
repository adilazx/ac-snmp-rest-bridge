"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const confFile = require('../config.json');
const URL = require("url");
const snmp_1 = require("./snmp");
function parseOID(oid) {
    return oid.includes(confFile.snmp.vendorOID[confFile.snmp.vendorOID.length - 1]) ? oid : confFile.snmp.vendorOID.slice(0).concat(...oid);
}
exports.parseOID = parseOID;
class Config {
    constructor(data) {
        this.data = data;
        this.mappings = [];
        if (!Array.isArray(data.mappings))
            throw new TypeError('Mappings must be an array');
        data.mappings.forEach(rawMapping => {
            let url = rawMapping.url;
            const parsedUrl = URL.parse(rawMapping.url, true);
            if (rawMapping.indexOid && !Array.isArray(rawMapping.indexOid))
                throw new TypeError('indexOid must be an array of integers');
            if (rawMapping.indexOid && (!rawMapping.indexName || typeof rawMapping.indexName !== 'string'))
                throw new TypeError('indexName must be a string');
            this.mappings.push({
                pathname: parsedUrl.pathname,
                pattern: new RegExp(parsedUrl.pathname, 'i'),
                params: Object.keys(parsedUrl.query),
                oids: rawMapping.oids,
                indexOid: rawMapping.indexOid ? parseOID(rawMapping.indexOid) : null,
                indexName: rawMapping.indexName
            });
        });
    }
    getMapping(pathname) {
        return this.mappings.find(m => m.pattern.test(pathname));
    }
    getOidPool(url) {
        const parsedUrl = URL.parse(url, true);
        const mapping = this.getMapping(parsedUrl.pathname);
        if (!mapping)
            throw new Error(`Could not found mapping for url ${url}`);
        mapping.params.forEach(p => {
            if (typeof parsedUrl.query[p] === 'undefined' || !parsedUrl.query[p])
                throw new Error(`Query param ${p} not provided`);
        });
        if (mapping.params.length !== Object.keys(parsedUrl.query).length)
            throw new Error(`Missing query param`);
        const pool = new snmp_1.OidPool(mapping.indexOid, mapping.indexName, parsedUrl.query);
        Object.keys(mapping.oids).filter(key => Array.isArray(mapping.oids[key])).map(key => {
            const oid = mapping.oids[key].map(l => typeof l === 'string' && l !== '$index' ? parsedUrl.query[l] : l);
            pool.addOidPair(key, parseOID(oid));
        });
        return pool;
    }
}
exports.Config = Config;
exports.config = new Config(confFile);
