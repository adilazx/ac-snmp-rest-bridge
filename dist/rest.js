"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const https = require("https");
const config_1 = require("./config");
const snmp_1 = require("./snmp");
const logger_1 = require("./logger");
function start(opts) {
    const app = express();
    config_1.config.mappings.forEach(map => {
        logger_1.default.info('Registering pathname', map.pathname);
        app.get(map.pathname, (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                logger_1.default.info('Querying url', req.originalUrl);
                const r = yield snmp_1.query(req.originalUrl);
                res.status(200).json(r);
            }
            catch (err) {
                logger_1.default.fatal(err);
                if (err instanceof Error)
                    return res.json({ description: err.message });
                else if (typeof err === 'string')
                    return res.json({ description: err });
                else
                    res.sendStatus(500);
            }
        }));
    });
    https.createServer(opts, app).listen(config_1.config.data.rest.port, config_1.config.data.rest.hostname, 1, () => {
        logger_1.default.info(`Listening on ${config_1.config.data.rest.hostname}:${config_1.config.data.rest.port}`);
    });
}
exports.default = start;
