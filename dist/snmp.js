"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const config_1 = require("./config");
const logger_1 = require("./logger");
const snmp = require('snmp-native');
class OidPool {
    constructor(indexOid, indexName, params) {
        this.indexOid = indexOid;
        this.indexName = indexName;
        this.params = params;
        this.oidPairs = new Map();
        this._oidMap = null;
        indexOid && indexOid.forEach(l => {
            if (!Number.isInteger(l))
                throw new TypeError('Index OID path element must be an integer value');
        });
    }
    addOidPair(key, oid) {
        oid.forEach(l => {
            if (isNaN(l) && l !== '$index')
                throw new Error('OID path element must be either $index or an integer value');
        });
        this.oidPairs.set(key, oid);
    }
    setIndexValues(val) {
        if (!Array.isArray(val))
            return;
        this._oidMap = null;
        val.forEach(v => {
            if (isNaN(v))
                throw new Error('Index value must be an integer');
        });
        this._indexValues = val;
    }
    get oidMap() {
        if (this._oidMap)
            return this._oidMap;
        if (this.indexOid && !this._indexValues)
            throw new Error('Index values not resolved');
        const map = new Map();
        for (let entry of this.oidPairs.entries()) {
            if (entry[1].includes('$index')) {
                this._indexValues.forEach(k => {
                    map.set(`${entry[0]}.${k}`, entry[1].slice(0).map(l => l === '$index' ? k : l));
                });
            }
            else
                map.set(entry[0], entry[1]);
        }
        this._oidMap = map;
        return map;
    }
    get oids() {
        const oids = [];
        for (let val of this.oidMap.values()) {
            oids.push(val);
        }
        return oids;
    }
    get keys() {
        const keys = [];
        for (let val of this.oidMap.keys()) {
            keys.push(val);
        }
        return keys;
    }
    mapValues(values) {
        for (let entry of values.entries()) {
            if (entry[1] === 'noSuchInstance')
                throw new Error(`No Such Instance, key: [${entry[0]}]`);
        }
        const r = {};
        for (let key in this.params) {
            r[key] = this.params[key];
        }
        if (this.indexName)
            r[this.indexName] = [];
        if (this.indexOid) {
            for (let entry of values.entries()) {
                const indexed = /^([\s\S]+)(?:\.)(\d+)$/.exec(entry[0]);
                if (indexed) {
                    const el = {};
                    el['id'] = indexed[2];
                    el[indexed[1]] = entry[1];
                    r[this.indexName].push(el);
                }
                else
                    r[entry[0]] = entry[1];
            }
        }
        else {
            for (let entry of values.entries()) {
                r[entry[0]] = entry[1];
            }
        }
        return r;
    }
}
exports.OidPool = OidPool;
function queryIndex(oid) {
    return new Promise((resolve, reject) => {
        try {
            logger_1.default.debug('Quering index oid', oid);
            const session = new snmp.Session(config_1.config.data.snmp);
            session.getSubtree({
                oid,
                combinedTimeout: config_1.config.data.snmp.combinedTimeout
            }, function (err, varbinds) {
                session.close();
                if (varbinds.length === 0)
                    return reject(new Error('Empty index values'));
                if (err)
                    return reject(err);
                const indexes = varbinds.map(vb => vb.value);
                logger_1.default.debug('Got indexes', indexes);
                resolve(indexes);
            });
        }
        catch (err) {
            reject(err);
        }
    });
}
function queryAll(oids, keys) {
    return new Promise((resolve, reject) => {
        logger_1.default.debug('Quering OIDs...');
        const session = new snmp.Session(config_1.config.data.snmp);
        session.getAll({
            oids,
            abortOnError: config_1.config.data.snmp.abortOnError,
            combinedTimeout: config_1.config.data.snmp.combinedTimeout
        }, function (err, varbinds) {
            session.close();
            if (err)
                return reject(err);
            const map = new Map();
            varbinds.forEach((vb, index) => {
                map.set(keys[index], vb.value);
            });
            logger_1.default.debug('Got values', map);
            resolve(map);
        });
    });
}
function query(url) {
    return new Promise((resolve, reject) => {
        try {
            const pool = config_1.config.getOidPool(url);
            const session = new snmp.Session(config_1.config.data.snmp);
            if (pool.indexOid) {
                queryIndex(pool.indexOid)
                    .then(indexes => {
                    pool.setIndexValues(indexes);
                    return queryAll(pool.oids, pool.keys);
                })
                    .then(values => {
                    resolve(pool.mapValues(values));
                })
                    .catch(err => reject(err));
            }
            else {
                queryAll(pool.oids, pool.keys)
                    .then(values => {
                    resolve(pool.mapValues(values));
                })
                    .catch(err => reject(err));
            }
        }
        catch (err) {
            reject(err);
        }
    });
}
exports.query = query;
