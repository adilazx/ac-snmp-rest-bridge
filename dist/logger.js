"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bunyan = require("bunyan");
const confFile = require('../config.json');
const logger = bunyan.createLogger(confFile.logger);
exports.default = logger;
