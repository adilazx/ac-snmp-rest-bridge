import * as fs from 'fs';
import * as path from 'path';
import './config';
import './logger';
import './snmp';
import rest from './rest';

const key = fs.readFileSync(path.join(__dirname, '..', 'cert', 'server.key'), 'utf8');
const cert = fs.readFileSync(path.join(__dirname, '..', 'cert', 'server.crt'), 'utf8');

rest({ key, cert });