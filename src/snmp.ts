import * as util from 'util';
import * as Promise from 'bluebird';
import { config, IMapping } from './config';
import logger from './logger';
const snmp = require('snmp-native');

export interface IVarBind {
    readonly type: number;
    readonly value: any;
    readonly oid: number[];
    readonly valueRaw: Buffer;
    readonly valueHex: string;
    readonly requestId: number;
    readonly receiveStamp: number;
    readonly sendStamp: number;
}

/**
 * OidPool
 */
export class OidPool {

    constructor(public indexOid: number[], public indexName: string, public params: { [key: string]: any }) {
        indexOid && indexOid.forEach(l => {
            if (!Number.isInteger(l)) throw new TypeError('Index OID path element must be an integer value');
        })
    }

    public addOidPair(key: string, oid: Array<number | '$index'>) {
        oid.forEach(l => {
            if (isNaN(l as any) && l !== '$index') throw new Error('OID path element must be either $index or an integer value');
        });

        this.oidPairs.set(key, oid);
    }

    protected oidPairs: Map<string, Array<number | '$index'>> = new Map();
    protected _indexValues: number[];
    protected _oidMap: Map<string, number[]> = null;

    public setIndexValues(val: number[]) {
        if (!Array.isArray(val)) return;
        this._oidMap = null;
        val.forEach(v => {
            if (isNaN(v)) throw new Error('Index value must be an integer');
        })
        this._indexValues = val;
    }

    public get oidMap(): Map<string, number[]> {
        if (this._oidMap) return this._oidMap;

        if (this.indexOid && !this._indexValues) throw new Error('Index values not resolved');
        const map: Map<string, number[]> = new Map();
        for (let entry of this.oidPairs.entries()) {
            if (entry[1].includes('$index')) {
                this._indexValues.forEach(k => {
                    map.set(`${entry[0]}.${k}`, entry[1].slice(0).map(l => l === '$index' ? k : l));
                });
            }
            else map.set(entry[0], entry[1] as number[]);
        }
        this._oidMap = map;
        return map;
    }

    public get oids(): number[][] {
        const oids: number[][] = [];
        for (let val of this.oidMap.values()) {
            oids.push(val);
        }
        return oids;
    }

    public get keys(): string[] {
        const keys: string[] = [];
        for (let val of this.oidMap.keys()) {
            keys.push(val);
        }
        return keys;
    }

    public mapValues(values: Map<string, any>): { [key: string]: any } {

        for (let entry of values.entries()) {
            if (entry[1] === 'noSuchInstance') throw new Error(`No Such Instance, key: [${entry[0]}]`);
        }

        const r: { [key: string]: any } = {};

        for (let key in this.params) {
            r[key] = this.params[key];
        }

        if (this.indexName) r[this.indexName] = [];

        if (this.indexOid) {
            for (let entry of values.entries()) {
                const indexed = /^([\s\S]+)(?:\.)(\d+)$/.exec(entry[0]);
                if (indexed) {
                    const el: { [key: string]: any } = {};
                    el['id'] = indexed[2];
                    el[indexed[1]] = entry[1];
                    r[this.indexName].push(el);
                }
                else r[entry[0]] = entry[1];
            }
        }
        else {
            for (let entry of values.entries()) {
                r[entry[0]] = entry[1];
            }
        }

        return r;
    }
}


function queryIndex(oid: number[]): Promise<number[]> {
    return new Promise<number[]>((resolve, reject) => {
        try {
            logger.debug('Quering index oid', oid);
            const session = new snmp.Session(config.data.snmp);
            session.getSubtree({
                oid,
                combinedTimeout: config.data.snmp.combinedTimeout
            }, function (err: Error, varbinds: IVarBind[]) {
                session.close();
                if (varbinds.length === 0) return reject(new Error('Empty index values'));
                if (err) return reject(err);
                const indexes = varbinds.map(vb => vb.value);
                logger.debug('Got indexes', indexes);
                resolve(indexes);
            })
        }
        catch (err) {
            reject(err);
        }
    })
}

function queryAll(oids: number[][], keys: string[]): Promise<Map<string, any>> {
    return new Promise<Map<string, any>>((resolve, reject) => {
        logger.debug('Quering OIDs...');
        const session = new snmp.Session(config.data.snmp);
        session.getAll({
            oids,
            abortOnError: config.data.snmp.abortOnError,
            combinedTimeout: config.data.snmp.combinedTimeout
        },
            function (err: Error, varbinds: IVarBind[]) {
                session.close();
                if (err) return reject(err);
                const map: Map<string, any> = new Map();
                varbinds.forEach((vb, index) => {
                    map.set(keys[index], vb.value);
                })
                logger.debug('Got values', map);
                resolve(map);
            })
    })
}


/**
 * SNMP Get
 */
export function query(url: string): Promise<{ [key: string]: any }> {

    return new Promise<{ [key: string]: any }>((resolve, reject) => {

        try {
            const pool = config.getOidPool(url);
            const session = new snmp.Session(config.data.snmp);

            if (pool.indexOid) {
                queryIndex(pool.indexOid)
                    .then(indexes => {
                        pool.setIndexValues(indexes);
                        return queryAll(pool.oids, pool.keys);
                    })
                    .then(values => {
                        resolve(pool.mapValues(values));
                    })
                    .catch(err => reject(err));

            }
            else {
                queryAll(pool.oids, pool.keys)
                    .then(values => {
                        resolve(pool.mapValues(values));
                    })
                    .catch(err => reject(err));

            }
        }
        catch (err) {
            reject(err);
        }
    })
}
