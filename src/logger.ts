import * as bunyan from 'bunyan';
import { IConfig } from './config';
const confFile: IConfig = require('../config.json');

const logger = bunyan.createLogger(confFile.logger);

export default logger;