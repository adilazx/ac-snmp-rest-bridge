const confFile = require('../config.json');
import * as bunyan from 'bunyan';
import * as URL from 'url';
import * as util from 'util';
import { OidPool } from './snmp';

export interface ILoggerConfig extends bunyan.LoggerOptions {

}

export interface ISnmpConfig {
    readonly vendorOID: number[];
    readonly host: string;
    readonly port: number;
    readonly community: string;
    readonly abortOnError: boolean;
    readonly combinedTimeout: number;
}

export interface IRestConfig {
    readonly port: number;
    readonly hostname: string;
}

export interface IRawOidMapping {
    [key: string]: string | Array<number | string>;
}

export interface IRawMapping {
    readonly url: string;
    readonly oids: IRawOidMapping;
    readonly indexOid?: number[];
    readonly indexName?: string;
}

export interface IOidMapping {
    [key: string]: Array<number>;
}

export interface IMapping {
    readonly pathname: string;
    readonly pattern: RegExp;
    readonly params: string[];
    readonly oids: IRawOidMapping;
    readonly indexOid?: number[];
    readonly indexName?: string;
}

export interface IConfig {
    readonly logger: ILoggerConfig;
    readonly snmp: ISnmpConfig;
    readonly rest: IRestConfig;
    readonly mappings: IRawMapping[];
}

export function parseOID(oid: number[]): number[] {
    return oid.includes(confFile.snmp.vendorOID[confFile.snmp.vendorOID.length - 1]) ? oid : confFile.snmp.vendorOID.slice(0).concat(...oid);
}

/**
 * Config
 */
export class Config {

    constructor(public data: IConfig) {
        if (!Array.isArray(data.mappings)) throw new TypeError('Mappings must be an array');

        data.mappings.forEach(rawMapping => {
            let url = rawMapping.url;
            const parsedUrl = URL.parse(rawMapping.url, true);

            if (rawMapping.indexOid && !Array.isArray(rawMapping.indexOid)) throw new TypeError('indexOid must be an array of integers');
            if (rawMapping.indexOid && (!rawMapping.indexName || typeof rawMapping.indexName !== 'string')) throw new TypeError('indexName must be a string');

            this.mappings.push({
                pathname: parsedUrl.pathname,
                pattern: new RegExp(parsedUrl.pathname, 'i'),
                params: Object.keys(parsedUrl.query),
                oids: rawMapping.oids,
                indexOid: rawMapping.indexOid ? parseOID(rawMapping.indexOid) : null,
                indexName: rawMapping.indexName
            });
        });
    }

    public mappings: IMapping[] = [];

    public getMapping(pathname: string): IMapping {
        return this.mappings.find(m => m.pattern.test(pathname));
    }

    public getOidPool(url: string): OidPool {
        const parsedUrl = URL.parse(url, true);
        const mapping = this.getMapping(parsedUrl.pathname);

        if (!mapping) throw new Error(`Could not found mapping for url ${url}`);

        mapping.params.forEach(p => {
            if (typeof parsedUrl.query[p] === 'undefined' || !parsedUrl.query[p]) throw new Error(`Query param ${p} not provided`);
        });
        if (mapping.params.length !== Object.keys(parsedUrl.query).length) throw new Error(`Missing query param`);

        const pool = new OidPool(mapping.indexOid, mapping.indexName, parsedUrl.query);

        Object.keys(mapping.oids).filter(key => Array.isArray(mapping.oids[key])).map(key => {
            const oid = (mapping.oids[key] as Array<number | string>).map(l => typeof l === 'string' && l !== '$index' ? parsedUrl.query[l] : l);
            pool.addOidPair(key, parseOID(oid));
        });

        return pool;
    }
}

export const config = new Config(confFile);
