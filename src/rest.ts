import * as express from 'express';
import * as https from 'https';
import { config } from './config';
import { query } from './snmp';
import logger from './logger';

export default function start(opts: https.ServerOptions) {

    const app = express();

    config.mappings.forEach(map => {
        logger.info('Registering pathname', map.pathname);
        app.get(map.pathname, async (req, res) => {
            try {
                logger.info('Querying url', req.originalUrl);
                const r = await query(req.originalUrl);
                res.status(200).json(r);
            }
            catch (err) {
                logger.fatal(err);
                if (err instanceof Error) return res.json({ description: err.message });
                else if (typeof err === 'string') return res.json({ description: err });
                else res.sendStatus(500);
            }
        })
    });

    https.createServer(opts, app).listen(config.data.rest.port, config.data.rest.hostname, 1, () => {
        logger.info(`Listening on ${config.data.rest.hostname}:${config.data.rest.port}`);
    });
}